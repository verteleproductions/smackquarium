﻿using UnityEngine;
using System.Collections;

public class TwoWayPlatforms : MonoBehaviour {

    public PlayerController player1;
    public PlayerController player2;



    public PlatformEffector2D platformEffector;


    public PlatformEffector2D platformEffectorPlayer1;
    public PlatformEffector2D platformEffectorPlayer2;

    public PlatformEffector2D platformEffectorNone;
    public PlatformEffector2D platformEffectorBoth;

    bool Player1GoThroughPlatform = false;
    bool Player2GoThroughPlatform = false;
    //small timer to reset the platform mask

    float player1DropDownTimer = 0.15f;
    float player1DropDownAccu = 0f;
    float player2DropDownTimer = 0.15f;
    float player2DropDownAccu = 0f;


    float player1InputTimer = 0.5f;
    float player2InputTimer = 0.5f;
    float player1InputAccu = 0;
    float player2InputAccu = 0;

    // Use this for initialization
    void Start () {
	
	}
	
    void Update()
    {
        if (player1InputTimer < player1InputAccu)
        {
            if (Input.GetButtonDown("Player1GoThroughPlatforms"))
            {
                Player1GoThroughPlatform = true;
                player1InputAccu = 0;
            }
        }

        if (player2InputTimer < player2InputAccu)
        {
            if (Input.GetButtonDown("Player2GoThroughPlatforms"))
            {
                Player2GoThroughPlatform = true;
                player2InputAccu = 0;
            }
        }
    }

	// Update is called once per frame
	void FixedUpdate () { 

        player2InputAccu += Time.deltaTime;
        player1InputAccu += Time.deltaTime;
        if ((Player1GoThroughPlatform == true) && (Player2GoThroughPlatform == true))
        {
            platformEffector.colliderMask = platformEffectorNone.colliderMask;
        }
        else if ((Player1GoThroughPlatform == true) && (Player2GoThroughPlatform == false))
        {
            platformEffector.colliderMask = platformEffectorPlayer1.colliderMask;
        }
        else if ((Player1GoThroughPlatform == false) && (Player2GoThroughPlatform == true))
        {
            platformEffector.colliderMask = platformEffectorPlayer2.colliderMask;
        }
        else
        {
            platformEffector.colliderMask = platformEffectorBoth.colliderMask;
        }

        if (Player1GoThroughPlatform)
        {

            player1DropDownAccu += Time.deltaTime;
            if (player1DropDownAccu > player1DropDownTimer)
            {
                Player1GoThroughPlatform = false;
                player1DropDownAccu = 0;
            }
        }
        if (Player2GoThroughPlatform)
        {
            
            player2DropDownAccu += Time.deltaTime;
            if (player2DropDownAccu > player2DropDownTimer)
            {
                Player2GoThroughPlatform = false;
                player2DropDownAccu = 0;
            }
        }
    }
}
