﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class WinConditionManager : MonoBehaviour {
    public GardenController player1;
    public GardenController player2;

	void Update (){
        if (player1.gardenState == GardenState.Four)
        {
            SceneManager.LoadScene("End Screen");
            //EndMenu.winner = Players.Player1;
        }

        else if (player2.gardenState == GardenState.Four)
        {
            SceneManager.LoadScene("End Screen");
            //EndMenu.winner = Players.Player2;
        }
          
    }
}
