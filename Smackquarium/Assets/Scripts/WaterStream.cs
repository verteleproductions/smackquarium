﻿using UnityEngine;
using System.Collections;

public class WaterStream : MonoBehaviour {

    ParticleGenerator particles;
    public KeyCode activationButton;

    float shake = 1f;

    // Use this for initialization
    void Start () {
        particles = GetComponent<ParticleGenerator>();
       

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(activationButton))
        {
            particles.enabled = true;
        }

        if (Input.GetKeyUp(activationButton))
        {
            particles.enabled = false;
        }

        if(particles.enabled)
        {
            if (shake > 0f)
            {
                transform.Translate(Vector3.one * Time.deltaTime);
                shake -= 0.16f;
            }
            else if (shake < 0f)
            {
                transform.Translate(Vector3.one * -Time.deltaTime);
                shake += 0.16f;
            }
            else if(shake == 0f)
            {
                transform.Translate(Vector3.one * -Time.deltaTime);
                shake += 0.16f;
            }
        }
    }
}
