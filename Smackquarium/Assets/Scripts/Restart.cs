﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour{
    static public void RestartScene(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
