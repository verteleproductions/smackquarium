﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public enum Players
{
    Player1,
    Player2,
    none
}

public class EndMenu : MonoBehaviour {

    public static Players winner = Players.none;

    string winnerPlayer;

    TextMesh[] meshes;
    TextMesh player1text;
    TextMesh player2text;
    TextMesh player1wins;
    TextMesh player2wins;
    float timer = 2;
    float timeraccu;

    public string player1Ready = "Waiting.. ";
    public string player2Ready = "Waiting.. ";
    string player1;
    string player2; 
    GUI menu;
    GUISkin menuSkin;

    void Start()
    {
        meshes = FindObjectsOfType<TextMesh>();

        for (int i = 0; i < meshes.Length; i++)
        {
            if (meshes[i].tag == "Player1")
            {
                player1text = meshes[i];
            }
            else if (meshes[i].tag == "Player2")
            {
                player2text = meshes[i];
            }

            if(meshes[i].tag == "Player1Hammer")
            {
                player1wins = meshes[i];

            }
        }
       
    }

    void OnGUI()
    {
    }

    void Update()
    {
        player1text.text = player1Ready;
        player2text.text = player2Ready;
        player1wins.text = winnerPlayer;

        if (winner == Players.Player1)
        {
            winnerPlayer = "Winner: Player 1";
        }
        else if(winner == Players.Player2)
        {   
            winnerPlayer = "Winner: Player 2";
        }
       
        if(Input.GetKeyDown(KeyCode.X))
        {
            player2Ready = "Ready";
        }
        else if(Input.GetKeyDown(KeyCode.C))
        {
            player2Ready = "Ready";
        }
        else if (Input.GetKeyDown(KeyCode.V))
        {
            player2Ready = "Ready";
        }
        if(Input.GetKeyDown(KeyCode.K))
        {
            player1Ready = "Ready";
        }
        else if(Input.GetKeyDown(KeyCode.J))
        {
            player1Ready = "Ready";
        }
        else if (Input.GetKeyDown(KeyCode.L))
        {
            player1Ready = "Ready";
        }

        if ((player1Ready == "Ready") && (player2Ready == "Ready"))
        {
            timeraccu += Time.deltaTime;
            if(timer < timeraccu)
                SceneManager.LoadScene("NewArena");
        }
    }
}
