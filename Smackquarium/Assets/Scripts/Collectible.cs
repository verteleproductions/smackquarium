﻿using UnityEngine;
using System.Collections;

public class Collectible : MonoBehaviour{
    Animator animator;
    new BoxCollider2D collider;

    public new AudioSource audio;

    LayerMask groundLayer;
    bool isGrounded = false;
    Transform groundPoint;

    public bool IsSpawned;

    Rigidbody2D rigidb;
    // Use this for initialization
    void Start (){
        groundPoint = GetComponentInChildren<Transform>();
        collider = GetComponent<BoxCollider2D>();
        rigidb = GetComponent<Rigidbody2D>();
        collider.enabled = false;
        animator = GetComponent<Animator>();
        audio.Play();
        groundLayer = LayerMask.GetMask("Ground");
    }
	
	// Update is called once per frame
	void Update (){
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Flower - IdleAnimation"))
            collider.enabled = true;
        else
            collider.enabled = false;
    }

    void FixedUpdate()
    {
        if(IsSpawned)
        {
            animator.SetBool("Spawned", true);
        }
        isGrounded = CheckGround(groundPoint, groundPoint);

        if (isGrounded)
        {
            rigidb.constraints = RigidbodyConstraints2D.FreezePositionX;
        }
    }

    public bool CheckGround(Transform groundPoint1, Transform groundPoint2)
    {
        if (Physics2D.OverlapCircle(groundPoint1.position, 0.10f, groundLayer))
            return true;
        else if (Physics2D.OverlapCircle(groundPoint2.position, 0.10f, groundLayer))
            return true;
        else
            return false;
    }
}
