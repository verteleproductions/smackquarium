﻿using UnityEngine;
using System.Collections;

public class NeutralTurtle : MonoBehaviour {

    public GardenController Player1Garden;
    public GardenController Player2Garden;

    string currFacing = "Right";
    public string facing = "Right";

    public Transform neutralStep;

    public Transform player1Step1;
    public Transform player1Step2;
    public Transform player1Step3;
    public Transform player2Step1;
    public Transform player2Step2;
    public Transform player2Step3;

    Animator animator;


    Transform targetTransform;

    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
        targetTransform = neutralStep;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if(Player1Garden.gardenState >= Player2Garden.gardenState)
        {
            if(Player1Garden.gardenState == GardenState.Two)
            {
                targetTransform = player1Step1;
            }
            if (Player1Garden.gardenState == GardenState.Three)
            {
                targetTransform = player1Step2;
            }
            if(Player1Garden.gardenState == GardenState.Four)
            {
                targetTransform = player1Step3;
            }
        }
        else if (Player1Garden.gardenState <= Player2Garden.gardenState)
        {
            if (Player2Garden.gardenState == GardenState.Two)
            {
                targetTransform = player2Step1;
            }
            if (Player2Garden.gardenState == GardenState.Three)
            {
                targetTransform = player2Step2;
            }
            if (Player2Garden.gardenState == GardenState.Four)
            {
                targetTransform = player2Step3;
            }
        }

        if((int)transform.position.x != (int)targetTransform.position.x)
        {
            if(transform.position.x > targetTransform.position.x)
            {
                currFacing = "Right";
                Flip();
                transform.position += new Vector3(Mathf.Lerp(0, targetTransform.position.x, Time.deltaTime / 2), 0, 0);
            }           
            else if (transform.position.x < targetTransform.position.x)
            {
                currFacing = "Left";
                Flip();
                transform.position += new Vector3(Mathf.Lerp(0, targetTransform.position.x, Time.deltaTime / 2), 0, 0);

            }

            animator.SetBool("IsIdle", false);
        }
        else
        {
            animator.SetBool("IsIdle", true);
        }
 
    }

    void Flip()
    {
        if (currFacing != facing)
        {
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
            facing = currFacing;
        }
    }
}
