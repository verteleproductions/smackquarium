﻿using UnityEngine;
using System.Collections;
using System;

public class FlowerSpawner : MonoBehaviour{
    [Header("Respawn Settings")]

    public float respawnTimer;
    float respawnAccu;

    public Transform spawnPoint1;
    public Transform spawnPoint2;
    public Transform spawnPoint3;
    public Transform spawnPoint4;
    public Transform spawnPoint5;

    Transform activeSpawnPoint;
    public int SpawnPointMax;

    System.Random spawnPointPicker = new System.Random();

    [Header("Prefab")]
    public Collectible collectiblePrefab;
    Collectible activeCollectible;

	void FixedUpdate ()
    {
        activeCollectible = FindObjectOfType<Collectible>();
        if (activeCollectible == null){
            respawnAccu += Time.deltaTime;
            if (respawnAccu > respawnTimer){
                int activateSpawnPoint = spawnPointPicker.Next(1, SpawnPointMax+1);
                if (activateSpawnPoint == 1)
                    activeSpawnPoint = spawnPoint1;
                else if (activateSpawnPoint == 2)
                    activeSpawnPoint = spawnPoint2;
                else if (activateSpawnPoint == 3)
                    activeSpawnPoint = spawnPoint3;
                else if (activateSpawnPoint == 4)
                    activeSpawnPoint = spawnPoint4;
                else if (activateSpawnPoint == 5)
                    activeSpawnPoint = spawnPoint5;
                Instantiate(collectiblePrefab, activeSpawnPoint.position, activeSpawnPoint.rotation);
                respawnAccu = 0;
            }
        }
	}
}
