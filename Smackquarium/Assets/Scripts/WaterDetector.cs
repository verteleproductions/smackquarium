﻿using UnityEngine;
using System.Collections;

public class WaterDetector : MonoBehaviour
{
    public Water parent;

    float moveupTimer = 1f;
    float moveupAccu = 0f;
    int particlesCollected = 0;

    void FixedUpdate()
    {
        moveupAccu += Time.deltaTime;
        if (moveupAccu > moveupTimer)
        {
            if (particlesCollected != 0)
            {
                parent.MoveUp(particlesCollected);
                particlesCollected = 0;
            }

        }

        

    }

    void OnParticleCollision(GameObject collision)
    {
        Debug.Log("ParticleCollision");
        if (collision.GetComponent<Rigidbody2D>() != null)
        {
            Debug.Log("RigidBody Found");
            transform.parent.GetComponent<Water>().Splash(transform.position.x, collision.GetComponent<Rigidbody2D>().velocity.y * collision.GetComponent<Rigidbody2D>().mass / 40f);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Rigidbody2D>() != null)
        {
            transform.parent.GetComponent<Water>().Splash(transform.position.x, collision.GetComponent<Rigidbody2D>().velocity.y * collision.GetComponent<Rigidbody2D>().mass / 40f);
        }
        if(collision.tag == "DynamicParticle")
        {
            particlesCollected++;
            Destroy(collision.gameObject);
        }
    }
}