﻿using UnityEngine;
using System.Collections;

public enum GardenState
{
    One,
    Two,
    Three,
    Four
}


public class GardenController : MonoBehaviour {
    public GardenState gardenState;
    public PlayerController parent;

    public GameObject Water;
	public GameObject flowers;
	void FixedUpdate () {
            gameObject.transform.position = new Vector3(transform.position.x, Water.transform.position.y, 0);
        }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == parent.tag)
        {
            int collectibles = parent.UseCollectibe();
			for (int i = 0; i < collectibles; i++)
				if (gardenState != GardenState.Four) {
					gardenState += 1;
					Instantiate (flowers, gameObject.transform.position, Quaternion.identity);
				}

        }

        if (collision.gameObject.tag == "Collectible")
        {
            gardenState++;
            DestroyObject(collision.gameObject);
			Instantiate(flowers, gameObject.transform.position, Quaternion.identity);
        }
    }
}
