﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class TurtleAnimator : MonoBehaviour{
    Animator bodyAnimator;
    Rigidbody2D rigidb;

    public GameObject arm;
    public GameObject hammer;

    public Vector2 move;

	void Start (){
        bodyAnimator = GetComponent<Animator>();
        rigidb = GetComponent<Rigidbody2D>();
    }

    public void SetBodyAnimationIdle(bool xValue, Vector2 xMove){

        bodyAnimator.SetBool("IsIdle", xValue);

        move = xMove;
        if(move.x < 0){
            move.x *= -1;
        }
        if (xValue == false && rigidb.velocity.y > -0.01f && rigidb.velocity.y < 0.01f){
            bodyAnimator.speed = move.x * 12;
        }
        else{
            bodyAnimator.speed = 1;
        }
    }

    public void SetDodge(bool xValue){
        bodyAnimator.SetBool("IsDodging", xValue);
    }
    
    public void SetJump(bool xValue){
        bodyAnimator.SetBool("IsJumping", xValue);
    }

    public void SetAttack(bool xValue){
        bodyAnimator.SetBool("IsAttacking", xValue);
    }

    public void SetFalling(bool xValue)
    {
        bodyAnimator.SetBool("IsFalling", xValue);
    }
}
