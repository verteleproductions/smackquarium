﻿using UnityEngine;
using System.Collections;

public class Hammer : MonoBehaviour{
    public PlayerController enemy;
    public PlayerController parent;
	public GameObject hitObject;

    public AudioSource audioSource;

    bool didHitSomething = false;

    bool hasAttacked = false;
    float attackWaiter = 0.4f;
    float attackAccu = 0;

    public AudioClip hitDodging;
    public AudioClip hitMiss;
    public AudioClip hitFatal;

    void FixedUpdate()
    {
        if (parent.isAttacking)
        {
            hasAttacked = true;
        }


        if (hasAttacked)
        {
            attackAccu += Time.deltaTime;
            if(attackAccu > attackWaiter)
            {
                if (!didHitSomething)
                {
                    audioSource.clip = hitMiss;
                }

                if(didHitSomething)
                {
                    audioSource.clip = hitFatal;
                }
                audioSource.Play();
                attackAccu = 0;
                didHitSomething = false;
                hasAttacked = false;
            }

        }


    }

    void OnTriggerEnter2D(Collider2D collision){
        if (collision.tag == enemy.tag){
            if(enemy.isDodging)
            {
                audioSource.clip = hitDodging;
            }
            enemy.TakeDamage(10);
            audioSource.Play();
            didHitSomething = true;
            Instantiate(hitObject, enemy.transform.position, Quaternion.identity);
        }

    }



}
