﻿using UnityEngine;
using System.Collections;

public enum MovementState { 
    Enable,
    Disable
}

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(InputController))]
public class PlayerController : MonoBehaviour{
    [Header("Movement Settings")]
    Rigidbody2D rigidb;
    Collider2D boxCollider;
    public MovementState movementState = MovementState.Enable;
    public float speed = 10f;
    public Vector2 jumpHeight;
    public Vector2 rollForce;
    public float rollTimer;
    public float dodgeTimer;
    float currentSpeed;
    Vector3 move;
    bool isGrounded = false;
    public bool isDodging = false;
    bool canRoll = false;
    bool canDodge = false;
    float rollAccu = 0f;
    float dodgeAccu = 0f;
    float slowMultiplier;
    string currFacing = "Right";
    public string facing = "Right";
    LayerMask groundLayer;
    public float slopeFriction;

	[Header("Particles Systems")]
	public GameObject dodgeParticleSystem;
	public GameObject runParticleSystem;
	public GameObject jumpParticleSystem;

    [Header("Attack Settings")]
    public float swingTimer;
    public float hammerSpeed;
    float hammerAccu;
    float swingAccu;
    public PolygonCollider2D hammer;
    bool canAttack;
    public bool isAttacking;

    [Header("Health Settings")]
    public int hitPoints;
    int maxHP;
    public int currentHP;
    public float deathTimer;
    float deathAccu;
    public Transform house;
    bool isDead;

    [Header("Collectible Settings")]
    public int maxCollectible;
    int collectibles;
    public float collectibleSlow;
    bool isCarryCollectible;
    public float flowerDropForce = 0;
    

    [Header("Player Input script")]
    public InputController playerInputs;

    [Header("Ground Point transforms")]
    public Transform groundPoint1;
    public Transform groundPoint2;

    [Header("Animator Script")]
    public new TurtleAnimator animation;

    [Header("Collectible Prefab")]
    public GameObject collectible;
    GameObject droppedCollectible;
    Rigidbody2D collectiblerb;

    [Header("Sound Controller")]
    public TurtleSoundController soundcontroller;
    public MusicController musicController;
    int flowerStage;



    //variable for keeping track of last velocity
    //used to stop particles from doing too much impact on the player

    Vector2 rigidbLastFrame;

    System.Random droppedCollectibleForce = new System.Random();

    void Start(){
        rigidb = GetComponent<Rigidbody2D>();
        maxHP = hitPoints;
        currentHP = maxHP;
        currentSpeed = speed;
        rigidbLastFrame = new Vector2(0, 0);
        flowerStage = 0;
        boxCollider = GetComponent<Collider2D>();
    }

    void FixedUpdate(){
        HandleMovement();
        if(movementState != MovementState.Disable){
            HandleCombat();
            HandleCollectibles();
        }


        rigidbLastFrame = rigidb.velocity;
    }

    void HandleMovement(){
        if (movementState == MovementState.Enable){
            rollAccu += Time.deltaTime;
            HandleGroundChecks();

            dodgeAccu += Time.deltaTime;
            if (dodgeTimer < dodgeAccu)
                canDodge = true;
            else
                canDodge = false;

            if (playerInputs.GetRollButton() && canRoll && ((move.x < 0) ||(move.x > 0)))
                Roll();
            else if (playerInputs.GetDodgeButton() && canDodge) {
                Dodge();
            }
            else {
                Move();
            }
            if (playerInputs.GetJumpButton() && isGrounded)
                Jump();
        }
        else if (movementState == MovementState.Disable){

            if(isCarryCollectible){
                isCarryCollectible = false;
                collectibles -= 1;
                Vector2 direction = new Vector2();
                direction.x = ((float)droppedCollectibleForce.Next(2, 10)) / 100;
             
                direction.y = 0.3f;
                droppedCollectible = (GameObject)Instantiate(collectible, transform.position, transform.rotation);
                if(facing == "Right")
                {
                    direction.x *= -flowerDropForce;
                }
                else if(facing == "Left")
                {
                    direction.x *= flowerDropForce;
                }
                collectiblerb = droppedCollectible.GetComponent<Rigidbody2D>();
                collectiblerb.AddForce(direction, ForceMode2D.Impulse);
            }

            if (isDead){
                transform.position = new Vector3(100, 100, 100);
                deathAccu += Time.deltaTime;
                if (deathAccu > deathTimer){
                    transform.position = house.position;
                    currentHP = maxHP;
                    deathAccu = 0;
                    movementState = MovementState.Enable;
                    isDead = false;
                }
            }
            else if (isDodging){
                dodgeAccu += Time.deltaTime;

                //When input is false, get out of dodge state.
                if (!playerInputs.GetDodgeButton())
                {
                    movementState = MovementState.Enable;
                    isDodging = false;
                    playerInputs.GetJumpButton();
                    dodgeAccu = 0;
                    animation.SetDodge(false);
                }
                //automatic undodge
                /*
                if (dodgeAccu > dodgeTimer)
                {



                }*/
            }
            else if (isAttacking){
                hammerAccu += Time.deltaTime;
                if (hammerAccu < hammerSpeed){
                    animation.SetAttack(true);
                    hammer.enabled = true;
                    Move();
                }
                else {
                    hammer.enabled = false;
                    hammerAccu = 0;
                    swingAccu = 0f;
                    animation.SetAttack(false);
                    isAttacking = false;
                    movementState = MovementState.Enable;
                }
            }
        }
    }

    void Move(){
        move = playerInputs.GetMovementAxis() * (currentSpeed * slowMultiplier * Time.deltaTime);

		if (move.x == 0f) {
			animation.SetBodyAnimationIdle (true, move);
			runParticleSystem.SetActive (false);
		}
		else{
                animation.SetBodyAnimationIdle(false, move);
			runParticleSystem.SetActive (true);

			}
		
        if (rigidb.velocity.y > 0.01f)
            animation.SetJump(true);

        else if(rigidb.velocity.y < 0)
        {
            animation.SetFalling(true);
            animation.SetJump(false);
        }

        if(isGrounded)
        {
            animation.SetFalling(false);
        }



        if(isGrounded)
        {
            NormalizeSlope();
        }

        transform.position += move;
        soundcontroller.WalkingFX(isGrounded, move);

        UpdateFacing();
    }

    void HandleGroundChecks(){
        isGrounded = CheckGround(groundPoint1, groundPoint2);

        if (isGrounded && rollTimer < rollAccu)
            canRoll = true;
        else
        {
            canRoll = false;
            animation.SetDodge(false);
        }
            
    }

    void UpdateFacing(){
        if (move.x > 0){
            currFacing = "Left";
            Flip();
        }
        else if (move.x < 0){
            currFacing = "Right";
            Flip();
        }
    }

    void Flip(){
        if (currFacing != facing){
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
            facing = currFacing;
        }
    }

    void Jump(){
        rigidb.velocity = jumpHeight;
    }

    void Roll(){
        float direction = 0f;
        if(facing == "Right")
            direction = -1;
        else
            direction = 1;
        rigidb.AddForce(rollForce * slowMultiplier * direction, ForceMode2D.Impulse);
        rollAccu = 0;
		Instantiate(dodgeParticleSystem, gameObject.transform.position, Quaternion.identity);
    }

    void HandleCombat(){
        if ((isDead = !CheckAlive()) == true)
            movementState = MovementState.Disable;


       
        swingAccu += Time.deltaTime;
        if (swingTimer < swingAccu)
            if (playerInputs.GetAttackButton()){
                isAttacking = true;
                movementState = MovementState.Disable;
            }
    }

    void Dodge(){
        movementState = MovementState.Disable;
        animation.SetDodge(true);
        isDodging = true;
        dodgeAccu = 0;
    }

    public void TakeDamage(int xDamage){
        if (!isDodging)
            currentHP -= xDamage;
        else
        {
            //if something should happen when dodging and hit. 
        }  
    }

    bool CheckAlive(){
        if(currentHP <= 0)
            return false;
        else
            return true;
    }

    void HandleCollectibles(){
        if(collectibles > 0)
        {
            isCarryCollectible = true;
            musicController.SetFlowerStage(gameObject.tag, flowerStage);
        }
            
            
        else
            isCarryCollectible = false;
        
        if(isCarryCollectible == true)
            slowMultiplier = collectibleSlow;
        else
            slowMultiplier = 1f;
    }

    public int UseCollectibe(){
        int collect = collectibles;
        collectibles = 0;
        flowerStage++;

        return collect;
    }

    void NormalizeSlope()   
    {
        if (isGrounded)
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.up, 1f, groundLayer);

            if (hit.collider != null && Mathf.Abs(hit.normal.x) > 0.1f)
            {
                rigidb.velocity = new Vector2(rigidb.velocity.x - (hit.normal.x * slopeFriction), rigidb.velocity.y);

                move.y += -hit.normal.x * Mathf.Abs(rigidb.velocity.x) * Time.deltaTime * (rigidb.velocity.x - hit.normal.x > 0 ? 1 : -1);
            }
        }
    }



    public bool CheckGround(Transform groundPoint1, Transform groundPoint2)
    {
        groundLayer = LayerMask.GetMask("Ground");
        if (Physics2D.OverlapCircle(groundPoint1.position, 0.10f, groundLayer))
            return true;
        else if (Physics2D.OverlapCircle(groundPoint2.position, 0.10f, groundLayer))
            return true;
        else
            return false;
    }


    void OnTriggerEnter2D(Collider2D collision){
        if (collision.gameObject.tag == "Collectible")
            if (collectibles < maxCollectible){
                collectibles++;
                DestroyObject(collision.gameObject);
            }
    }


    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "DynamicParticle")
        {
            rigidb.velocity = rigidbLastFrame - Vector2.one * rigidb.gravityScale*Time.deltaTime;
            Physics2D.IgnoreCollision(collision.collider, boxCollider, true);
        }
        
    }
}