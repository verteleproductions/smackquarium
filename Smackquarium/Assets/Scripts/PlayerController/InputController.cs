﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour {
    string MovementAxis;
    string AttackButton;
    string JumpButton;
    string DodgeButton;
    string RollButton;

    float jumptimer = 0.3f;
    float jumpaccu = 0;

    Vector3 movementAxis;
    bool attackButton;
    bool jumpButton;
    bool dodgeButton;
    bool rollButton;

    void Start (){
        if (gameObject.tag == "Player1"){
            MovementAxis = "HorizontalPlayer1";
            AttackButton = "AttackPlayer1";
            JumpButton = "JumpPlayer1";
            DodgeButton = "DodgePlayer1";
            RollButton = "RollPlayer1";
        }
        else{
            MovementAxis = "HorizontalPlayer2";
            AttackButton = "AttackPlayer2";
            JumpButton = "JumpPlayer2";
            DodgeButton = "DodgePlayer2";
            RollButton = "RollPlayer2";
        }
    }

	void Update (){
        jumpaccu += Time.deltaTime;
        if(Input.GetButtonDown(JumpButton) && (jumpaccu > jumptimer))
        {
            jumpButton = true;
            jumpaccu = 0;
        }
        
        if (Input.GetAxis(AttackButton) != 0)
            attackButton = true;
        else
            attackButton = false;
        if (Input.GetAxis(DodgeButton) != 0)
            dodgeButton = true;
        else
            dodgeButton = false;
        if (Input.GetAxis(RollButton) != 0)
            rollButton = true;
        else
            rollButton = false;
        movementAxis.x = Input.GetAxis(MovementAxis);
    }

    public Vector3 GetMovementAxis(){
        return movementAxis; 
    }
    public bool GetJumpButton(){
        bool returnbool = jumpButton;
        jumpButton = false;
        return returnbool;
    }
    public bool GetAttackButton(){
        return attackButton;
    }
    public bool GetDodgeButton(){
        return dodgeButton;
    }
    public bool GetRollButton(){
        return rollButton;
    }

}
