﻿using UnityEngine;
using System.Collections;

public class HammerSoundController : MonoBehaviour {
    public AudioSource audioSource;

    public AudioClip attackHit;
    public AudioClip attackMiss;
    public AudioClip attackHitBlock;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
