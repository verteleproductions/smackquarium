﻿using UnityEngine;
using System.Collections;

public class Player2WaterStream : MonoBehaviour
{

    public Transform waterPoint1;
    public Transform waterPoint2;
    public Transform waterPoint3;
    public Transform waterPoint4;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            transform.position = waterPoint1.position;
            GetComponent<ParticleGenerator>().enabled = true;

        }
        else if (Input.GetKey(KeyCode.S))
        {
            transform.position = waterPoint2.position;
            GetComponent<ParticleGenerator>().enabled = true;

        }
        else if (Input.GetKey(KeyCode.D))
        {
            transform.position = waterPoint3.position;
            GetComponent<ParticleGenerator>().enabled = true;

        }
        else if (Input.GetKey(KeyCode.F))
        {
            transform.position = waterPoint4.position;
            GetComponent<ParticleGenerator>().enabled = true;

        }
        else
        {
            GetComponent<ParticleGenerator>().enabled = false;
        }
    }
}
