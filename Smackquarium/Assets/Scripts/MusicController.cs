﻿using UnityEngine;
using System.Collections;

public struct FlowerCounter{
    public string player;
    public int flowerStage;
    public FlowerCounter(string tag, int stage){
        player = tag;
        flowerStage = stage;
    }
}

public enum MusicStage{
    Stage_None,
    Stage_One,
    Stage_Two,
    Stage_Three,
    Stage_Four,
    Stage_Five
}


public class MusicController : MonoBehaviour {

    public DemoPlayer eliasPlayer;
    public MusicStage musicStage;

    FlowerCounter player1;
    FlowerCounter player2;


    // Use this for initialization
    void Start () {
        musicStage = MusicStage.Stage_None;
        player1 = new FlowerCounter("Player1", -1);
        player2 = new FlowerCounter("Player2", -1);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (musicStage < MusicStage.Stage_One){
            if ((player1.flowerStage == 1) || (player2.flowerStage == 1)){
                musicStage = MusicStage.Stage_One;
            }
        }

        if(musicStage < MusicStage.Stage_Two){
            if ((player1.flowerStage == 2) || (player2.flowerStage == 2)){
                musicStage = MusicStage.Stage_Two;
            }
        }

        if(musicStage < MusicStage.Stage_Three){
            if ((player1.flowerStage == 3) || (player2.flowerStage == 3)){
                musicStage = MusicStage.Stage_Three;
            }
        }

        if (musicStage == MusicStage.Stage_One){
            eliasPlayer.SetLevel(8, 8, 100, 0, 1, 0);
        }
        else if (musicStage == MusicStage.Stage_Two){
            eliasPlayer.SetLevel(12, 12, 100, 0, 1, 0);
        }
        else if (musicStage == MusicStage.Stage_Three){
            eliasPlayer.SetLevel(20, 20, 100, 0, 1, 0);
        }
    }

    public void SetFlowerStage(string playerTag, int stage){
        if(player1.player == playerTag){
            player1.flowerStage = stage;
        }
        else if(player2.player == playerTag){
            player2.flowerStage = stage;
        }
    }
}
