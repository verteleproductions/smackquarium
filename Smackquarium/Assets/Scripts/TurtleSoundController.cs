﻿using UnityEngine;
using System.Collections;

public class TurtleSoundController : MonoBehaviour {
    bool isGrounded;
    Vector2 move;

    float walkAccu;
    public float walkSync;

    public AudioSource audioSource;

    public AudioClip walkingSound;
    public AudioClip jumpingSound;
    public AudioClip landingSound;
    public AudioClip dashSound;
    public AudioClip pickFlowerSound;
    public AudioClip loseFlowerSound;
    public AudioClip getHitSound;
    public AudioClip changeWeaponSound;

    public AudioClip dodgeSound;
    public AudioClip attackSound;



	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame

    void FixedUpdate()
    {
        if (isGrounded)
        {
            if(move.x > 0)
            {
                walkAccu += Time.deltaTime * 10 * move.x;
            }
            if (move.x < 0)
            {
                walkAccu += Time.deltaTime * 10 * -move.x;
            }
        }

        if(walkAccu > walkSync)
        {
            if(audioSource.isPlaying != true)
            {
                audioSource.clip = walkingSound;
                audioSource.Play();
                walkAccu = 0;
            }
        }
    }

    public void WalkingFX(bool xValue, Vector2 xMove)
    {
        isGrounded = xValue;
        move = xMove;
    }

}
